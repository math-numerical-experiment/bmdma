load("f90") $
:lisp (setq *f90-output-line-length-max* 1000000000)

/* d: dose */
/* a = x1, b = x2, g = x3: parameters */
/* func(a, b, g, d) := g + (1 - g) * 1/2 * (1 + erf((a + b * log(d)) / sqrt(2))); */
/* gradef(Phi(x), 1 / sqrt(2 * %pi) * exp(-x^2 / 2)); */
gradef(Phi(x), PhiDiff(x));
gradef(PhiInv(x), 1 / subst(PhiInv(x), x, diff(Phi(x), x)));
func(a, b, g, d) := g + (1 - g) * Phi(a + b * log(d));
f90(func(x1, x2, x3, d));

/* gradient */
grad(a, b, g, d) := ratsimp([diff(func(a, b, g, d), a), diff(func(a, b, g, d), b), diff(func(a, b, g, d), g)]);
f90(grad(x1, x2, x3, d));

/* m: BMD */
/* r: BMR */
/* m = x1, a = x2, g = x3: parameters */
A: (PhiInv((1 - r) * g + r) - log(m) * b);

func2(m, b, g, r, d) := ratsimp(ev(func(A, b, g, d)));
f90(func2(x1, x2, x3, r, d));

/* gradient */
grad2(m, b, g, r, d) := ratsimp([diff(func2(m, b, g, r, d), m), diff(func2(m, b, g, r, d), b), diff(func2(m, b, g, r, d), g)]);
f90(grad2(x1, x2, x3, r, d));
