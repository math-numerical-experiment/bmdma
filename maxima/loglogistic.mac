load("f90") $
:lisp (setq *f90-output-line-length-max* 1000000000)

/* d: dose */
/* a = x1, b = x2, g = x3: parameters */

func(a, b, g, d) := g + (1 - g) / (1 + exp(- a - b * log(d)));
f90(func(x1, x2, x3, d));
tex(func(x1, x2, x3, d));
assume(x2 > 0);
f90(limit(func(x1, x2, x3, d), d, 0, plus));

/* gradient */
grad(a, b, g, d) := ratsimp([diff(func(a, b, g, d), a), diff(func(a, b, g, d), b), diff(func(a, b, g, d), g)]);
f90(grad(x1, x2, x3, d));
f90(limit(grad(x1, x2, x3, d), d, 0, plus));

/* m: BMD */
/* r: BMR */
/* m = x1, a = x2, g = x3: parameters */
assume(b > 0);
assume(r > 0);
bmd(a, b, g, r) := solve((func(a, b, g, m) - limit(func(a, b, g, d), d, 0, plus)) / (1 - limit(func(a, b, g, d), d, 0, plus)) = r, m);
f90(bmd(x1, x2, x3, r));

sol: solve((func(a, b, g, m) - limit(func(a, b, g, d), d, 0, plus)) / (1 - limit(func(a, b, g, d), d, 0, plus)) = r, a);
A: ev(ratsimp(rhs(sol[1])));

func2(m, b, g, r, d) := ratsimp(ev(func(A, b, g, d)));
f90(func2(x1, x2, x3, r, d));
f90(limit(func2(x1, x2, x3, r, d), d, 0, plus));

/* gradient */
grad2(m, b, g, r, d) := ratsimp([diff(func2(m, b, g, r, d), m), diff(func2(m, b, g, r, d), b), diff(func2(m, b, g, r, d), g)]);
f90(grad2(x1, x2, x3, r, d));
f90(limit(grad2(x1, x2, x3, r, d), d, 0, plus));
