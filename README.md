# BMDMA

## Installation

We can install BMDMA from gitlab by using devtools.
If we has not installed devtools then first we install it.

``` r
install.packages("devtools")
```

To install BMDMA, we execute the following.

``` r
devtools::install_git("https://gitlab.com/math-numerical-experiment/bmdma.git")
```

## Usage examples

### GUI

BMDMA has GUI that is implemented by tcltk.

``` r
bmdma::bmdma_gui()
```

## Links

- [GitLab repository](https://gitlab.com/math-numerical-experiment/bmdma)
- [GitLab Issues](https://gitlab.com/math-numerical-experiment/bmdma/-/issues)
- [R package](https://gitlab.com/math-numerical-experiment/bmdma/-/packages)
- [Web page (in Japanese)](https://math-numerical-experiment.gitlab.io/bmdma/)

## Copyright and license

BMDMA Copyright (C) 2021 Kyoto University and Shiga University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Acknowledgments

This study was supported by a grant from the Food Safety Commission,
Cabinet Office, Government of Japan (Research Program for Risk
Assessment Study on Food Safety, No 1907).
