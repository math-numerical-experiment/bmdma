context("test-recommend-model")

test_that("Return unusable model", {
    expect({
        df <- data.frame(
            convergence = c(0, 0, 1, 0),
            BMD = c(1, 2, NA, -1),
            BMDL = c(NA, 1, NA, -7),
            row.names = c("a", "b", "c", "d")
        )
        all(get_row_number_of_unusable_model(df) == determine_unusable_model(df, 1:nrow(df))[["index"]])
    }, "Return unusable model")
})

test_that("Get lowest non-zero dose", {
    expect({
        df <- data.frame(
            response = c(0, 10, 5, 0),
            dose = c(50, 100, 70, 30),
            row.names = c("a", "b", "c", "d")
        )
        get_lowest_non_zero_dose(df) == 70
    }, "Get lowest non-zero dose")
})

test_that("Recommend a model among all models", {
    expect({
        ## bmr <- 0.1
        ## ma_type <- c("MA_ALL", "MA_3")
        ## df_result <- bmdma_bmd_ul(test_data[[1]], bmr, list_model = "ALL", list_ma_type = ma_type)
        ## write.csv(df_result, "data/result1.csv")
        df_result <- test_result[[1]]
        df <- bmdma_recommend_model(df_result, test_data[[1]])
        cols <- setdiff(rownames(df), "MA_3")
        (df["MA_3", "recommendation"] == "Recommended") && all(is.na(df[cols, "recommendation"]))
    }, "Can not recommend a model among all models")
})

test_that("Recommend a model except for model averaging", {
    expect({
        df_result <- test_result[[1]][,]
        df_result <- df_result[setdiff(rownames(df_result), c("MA_3", "MA_ALL")),]
        df <- bmdma_recommend_model(df_result, test_data[[1]])
        cols <- setdiff(rownames(df), "logistic")
        (df["logistic", "recommendation"] == "Recommended") && all(is.na(df[cols, "recommendation"]))
        TRUE
    }, "Can not recommend a model except for model averaging")
})

test_that("Set questionable and warning", {
    expect({
        reference_value = list(
            questionable = list(
                "BMDL/BMD" = 1/20,
                "BMD/lowest_non_zero_dose" = 1/10,
                "BMDL/lowest_non_zero_dose" = 1/10,
                "AIC_difference" = 3,
                "ratio_BMDU_BMDL_to_median" = 10
            ),
            warning = list(
                "BMDL/BMD" = 1/5,
                "BMD/lowest_non_zero_dose" = 1/3,
                "BMDL/lowest_non_zero_dose" = 1/3,
                "BMD/highest_dose" = 1,
                "BMDL/highest_dose" = 1,
                "BMDU_not_estimated" = TRUE,
                "fraction_failed_iterations" = 0.01
            )
        )

        df_result <- test_result[[2]]
        df <- bmdma_recommend_model(df_result, test_data[[1]], reference_value = reference_value)

        q <- df[["quality_bin"]]
        result_unusable <- rownames(df)[!is.na(q) & (q == "unusable")]
        result_questionable <- rownames(df)[!is.na(q) & (q == "questionable")]
        result_warning <- rownames(df)[!is.na(q) & (q == "warning")]

        ans_unusable <- c("restricted_loglogistic", "restricted_gamma", "restricted_weibull", "restricted_multistage2", "restricted_multistage3")
        ans_questionable <- c("multistage3", "restricted_logistic", "restricted_probit", "restricted_quantallinear")
        ans_warning <- c("gamma", "logprobit", "restricted_logprobit")

        setequal(ans_unusable, result_unusable) && setequal(ans_questionable, result_questionable) &&
            setequal(ans_warning, result_warning)
    }, "Can not set questionable and warning")
})
