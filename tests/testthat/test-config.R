context("test-config")

test_that("Set value by bmdma_configure", {
    expect({
        val <- 10
        bmdma_configure("set", c("bootstrap", "sampling"), val)
        val_new <- bmdma_config[["bootstrap"]][["sampling"]]
        bmdma_configure("reset")
        val_new == val
    }, "Can not set value by bmdma_configure.")
})

test_that("Reset value by bmdma_configure", {
    expect({
        val <- 10
        bmdma_configure("set", c("bootstrap", "sampling"), val)
        bmdma_configure("reset")
        bmdma_config[["bootstrap"]][["sampling"]] == bmdma_config_default[["bootstrap"]][["sampling"]]
    }, "Can not reset value by bmdma_configure.")
})
