##' @description
##' ## Multistage3 (three-stage)
##' bmdma_model\[\["multistage3"\]\] is the definition of multistage3 model:
##' \deqn{p(d) = {x_{4}+\left(1-x_{4}\right)\,\left(1-e^{-x_{1}\,d-x_{2}\,d^2-x_{3}\,d^3}\right)}.}
##' The parameter \eqn{x_4} is searched in \eqn{[0, 1 - \epsilon]} by default.
##' The parameters \eqn{(x_1, x_2, x_3, x_4)} are searched in
##' \eqn{[0 + \epsilon, \infty) \times [0 + \epsilon, \infty) \times [0 + \epsilon, \infty)
##' \times [0, 1 - \epsilon]} if optional restriction is used.
##' @name bmdma_model
##' @rdname bmdma_model
NULL

bmdma_model[["multistage3"]] <- list(
    ## g + (1 - g) * (1 - exp(- a * d - b * d^2 - c * d^3))
    ## x = (a, b, c, g)
    name = "multistage3",
    number_of_parameters = 4,
    func = function(x, d) {
        p <- x[4] + (1 - x[4]) * (1 - exp(- d * (x[1] + d * (x[2] + x[3] * d))))
        ind_value_neg <- (p < 0)
        p[ind_value_neg] <- 0
        ind_value_lt1 <- (p > 1)
        p[ind_value_lt1] <- 1
        return(p)
    },
    grad = function(x, d) {
        v1 <- exp(-d * (x[1] + d * (x[2] + x[3] * d)))
        v2 <- (1 - x[4]) * v1 * d
        v3 <- v2 * d
        return(c(v2, v3, v3 * d, v1))
    },
    bmd = function(x, r) {
        v1 <- log(1-r)
        v2 <- x[3]**2
        v3 <- x[2]**2
        v4 <- x[2]**3
        v5 <- ((-1.0)/2.0-(sqrt(3)*(1i))/2.0)
        v6 <- ((sqrt(3)*(1i))/2.0+(-1.0)/2.0)
        v7 <- sqrt(as.complex(27*v1**2*v2+(4*x[1]**3-18*v1*x[1]*x[2])*x[3]+4*v1*v4-x[1]**2*v3))
        v8 <- (((3**((-3.0)/2.0)*v7)/v2)/2.0+((x[1]*x[2])/v2-(3*log((-r)+1))/x[3])/6.0-(v4/x[3]**3)/27)
        v9 <- ((x[1]/x[3])/3.0-(v3/v2)/9.0)*v8**((-1.0)/3.0)
        v10 <- v8**(1.0/3.0)
        v11 <- (x[2]/x[3])/3.0
        sols <- c(v5*v10-v6*v9-v11, v6*v10-v5*v9-v11, v10-v9-v11)
        sols <- sols[is.finite(sols)]
        ind <- sapply(sols, function(v) { return((Im(v) < 1e-8) && Re(v) > 0.0) })
        if (any(ind)) {
            return(min(sapply(sols[ind], Re)))
        } else {
            sols <- sols[order(sapply(sols, function(v) { abs(Im(v)) }))]
            for (v in sols) {
                if (Re(v) > 0.0) {
                    return(Re(v))
                }
            }
            return(NA)
        }
    },
    default_restriction = list(
        lower = c(-Inf, -Inf, -Inf, 0),
        upper = c(Inf, Inf, Inf, 1),
        endpoint = list(
            lower = c(TRUE, TRUE, TRUE, TRUE),
            upper = c(TRUE, TRUE, TRUE, FALSE)
        )
    ),
    optional_restriction = list(
        lower = c(0, 0, 0, 0),
        upper = c(Inf, Inf, Inf, 1),
        endpoint = list(
            lower = c(TRUE, TRUE, TRUE, TRUE),
            upper = c(TRUE, TRUE, TRUE, FALSE)
        )
    ),
    init = function(df, restriction) {
        ratio <- df[["response"]] / df[["N"]]
        ratio <- ratio[order(ratio)]
        dose <- df[["dose"]]
        dose <- dose[order(dose)]
        if (length(ratio) < 2) {
            x4 <- 0.0
            ratio_75 <- ratio * 0.75
            dose_75 <- dose * 0.75
        } else {
            x4 <- max((ratio[2] * dose[1] - ratio[1] * dose[2]) / (dose[1] - dose[2]), 0.0001)
            ratio_min <- max(ratio[1], 0.01)
            ratio_75 <- ratio[1] * 0.25 + ratio[length(ratio)] * 0.75
            v <- ratio - ratio_75
            i <- which(v > 0)[1]
            p <- v[i - 1] / (v[i - 1] - v[i])
            ratio_75 <- ratio[i - 1] * (1 - p) + ratio[i] * p
            dose_75 <- dose[i - 1] * (1 - p) + dose[i] * p
        }
        x1 <- -log((1.0 - ratio_75) / (1.0 - x4)) / (dose_75 * (1.0 + dose_75 * (1.0 + dose_75)))
        prm <- c(x1, x1, x1, x4)
        if (prm[1] < 0.0) {
            prm[1] <- 0.0001
            prm[2] <- 0.0001
            prm[3] <- 0.0001
        }
        return(prm)
    }
)
