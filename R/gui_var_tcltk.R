##' @import stringr

var_tcltk_initialize <- function() {
    list_model_normal <- sapply(names(bmdma_model), function(name) {
        return(tclVar(0))
    }, simplify = FALSE, USE.NAMES = TRUE)
    list_model_restricted <- sapply(names(bmdma_model), function(name) {
        return(tclVar(0))
    }, simplify = FALSE, USE.NAMES = TRUE)
    names(list_model_restricted) <- sapply(names(bmdma_model), function(name) {
        return(paste0("restricted_", name))
    }, simplify = TRUE)
    list_reference_value_enabled <- sapply(names(bmdma_config[["reference_value"]]), function(value_type) {
        return(sapply(names(bmdma_config[["reference_value"]][[value_type]]), function(value_name) {
            return(tclVar(0))
        }, simplify = FALSE, USE.NAMES = TRUE))
    }, simplify = FALSE, USE.NAMES = TRUE)
    list_reference_value <- sapply(names(bmdma_config[["reference_value"]]), function(value_type) {
        return(sapply(names(bmdma_config[["reference_value"]][[value_type]]), function(value_name) {
            tclVar(bmdma_config[["reference_value"]][[value_type]][[value_name]])
        }, simplify = FALSE, USE.NAMES = TRUE))
    }, simplify = FALSE, USE.NAMES = TRUE)
    return(list(
        "data_file" = tclVar(""),
        "data_column" = list("dose" = tclVar(""), "N" = tclVar(""), "response" = tclVar("")),
        "bmr" = tclVar(10),
        "list_model" = c(list_model_normal, list_model_restricted),
        "list_ma" = list(
            "MA_ALL" = tclVar(0),
            "MA_3" = tclVar(0)
        ),
        "risk_type" = tclVar("extra"),
        "sampling" = tclVar(""),
        "confidence_level" = tclVar(""),
        "reference_value_enabled" = list_reference_value_enabled,
        "reference_value" = list_reference_value,
        "cluster" = tclVar("on"),
        "message" = list(
            "main.label.invalid_data_file" = tclVar("")
        )
    ))
}

var_tcltk_set_boolean_from_list <- function(var_tcltk, vals, key) {
    names_true <- vals[[key]]
    if (!is.null(names_true)) {
        for (name in names_true) {
            tclvalue(var_tcltk[[key]][[name]]) <- 1
        }
        for (name in setdiff(names(var_tcltk[[key]]), names_true)) {
            tclvalue(var_tcltk[[key]][[name]]) <- 0
        }
    }
}

var_tcltk_set_func_without_conversion <- function(var_tcltk, vals, key) {
    if (!is.null(vals[[key]])) {
        tclvalue(var_tcltk[[key]]) <- vals[[key]]
    }
}

var_tcltk_set_func <- list(
    "data_file" = var_tcltk_set_func_without_conversion,
    "data_column" = function(var_tcltk, vals, key) {
        v <- vals[[key]]
        if (!is.null(v)) {
            for (col_file in names(v)) {
                col_original <- v[[col_file]]
                if (!is.null(var_tcltk[[key]][[col_original]])) {
                    tclvalue(var_tcltk[[key]][[col_original]]) <- col_file
                }
            }
        }
    },
    "bmr" = var_tcltk_set_func_without_conversion,
    "risk_type" = var_tcltk_set_func_without_conversion,
    "list_model" = var_tcltk_set_boolean_from_list,
    "list_ma" = var_tcltk_set_boolean_from_list,
    "sampling" = function(var_tcltk, vals, key) {
        if (!is.null(vals[[key]])) {
            tclvalue(var_tcltk[[key]]) <- as.character(as.integer(vals[[key]]))
        }
    },
    "confidence_level" = function(var_tcltk, vals, key) {
        if (!is.null(vals[[key]])) {
            tclvalue(var_tcltk[[key]]) <- as.character(vals[[key]])
        }
    },
    "reference_value" = function(var_tcltk, vals, key) {
        if (!is.null(vals[[key]])) {
            for (value_type in names(vals[[key]])) {
                for (value_name in names(vals[[key]][[value_type]])) {
                    tclvalue(var_tcltk[[key]][[value_type]][[value_name]]) <- vals[[key]][[value_type]][[value_name]]
                }
            }
        }
    },
    "reference_value_enabled" = function(var_tcltk, vals, key) {
        if (!is.null(vals[[key]])) {
            for (value_type in names(vals[[key]])) {
                var_tcltk_set_boolean_from_list(var_tcltk[[key]], vals[[key]], value_type)
            }
        }
    },
    "cluster" = function(var_tcltk, vals, key) {
        if (vals[[key]]) {
            tclvalue(var_tcltk[[key]]) <- "on"
        } else {
            tclvalue(var_tcltk[[key]]) <- "off"
        }
    }
)

var_tcltk_set <- function(var_tcltk, vals) {
    for (key in names(vals)) {
        func <- var_tcltk_set_func[[key]]
        if (!is.null(func)) {
            func(var_tcltk, vals, key)
        }
    }
}

var_tcltk_get_vector_of_true_value <- function(var_tcltk, key) {
    tcltk_boolean_list <- var_tcltk[[key]]
    keys_true <- vector()
    for (name in names(tcltk_boolean_list)) {
        if (as.integer(tclvalue(tcltk_boolean_list[[name]])) == "1") {
            keys_true[length(keys_true) + 1] <- name
        }
    }
    return(keys_true)
}

var_tcltk_get_func_character <- function(var_tcltk, key) {
    v <- try(tclvalue(var_tcltk[[key]]), silent = TRUE)
    if (class(v) == "try-error") {
        v <- NA
    }
    return(v)
}

var_tcltk_get_func <- list(
    "data_file" = var_tcltk_get_func_character,
    "data_column" = function(var_tcltk, key) {
        vals <- list()
        for (col_file in names(var_tcltk[[key]])) {
            col_original <- as.character(tclvalue(var_tcltk[[key]][[col_file]]))
            if (str_length(col_original) > 0) {
                vals[[col_original]] <- col_file
            }
        }
        return(vals)
    },
    "bmr" = function(var_tcltk, key) {
        return(as.integer(tclvalue(var_tcltk[[key]])))
    },
    "list_model" = var_tcltk_get_vector_of_true_value,
    "list_ma" = var_tcltk_get_vector_of_true_value,
    "risk_type" = var_tcltk_get_func_character,
    "sampling" = function(var_tcltk, key) {
        v <- try(as.integer(tclvalue(var_tcltk[[key]])), silent = TRUE)
        if (class(v) == "try-error") {
            v <- NA
        }
        return(v)
    },
    "confidence_level" = function(var_tcltk, key) {
        v <- try(as.numeric(tclvalue(var_tcltk[[key]])), silent = TRUE)
        if (class(v) == "try-error") {
            v <- NA
        }
        return(v)
    },
    "reference_value" = function(var_tcltk, key) {
        vals <- list()
        for (value_type in names(var_tcltk[[key]])) {
            vals[[value_type]] <- list()
            list_enabled <- var_tcltk[[key]][[value_type]]
            for (value_name in names(list_enabled)) {
                v <- try(as.numeric(tclvalue(list_enabled[[value_name]])), silent = TRUE)
                if (class(v) == "try-error") {
                    v <- NA
                }
                vals[[value_type]][[value_name]] <- v
            }
        }
        return(vals)
    },
    "reference_value_enabled" = function(var_tcltk, key) {
        vals <- list()
        for (value_type in names(var_tcltk[[key]])) {
            vals[[value_type]] <- var_tcltk_get_vector_of_true_value(var_tcltk[[key]], value_type)
        }
        return(vals)
    },
    "cluster" = function(var_tcltk, key) {
        return(tclvalue(var_tcltk[[key]]) == "on")
    }
)

var_tcltk_get <- function(var_tcltk, keys = NULL) {
    if (is.null(keys)) {
        keys <- names(var_tcltk_get_func)
    }
    vals <- list()
    for (key in keys) {
        func <- var_tcltk_get_func[[key]]
        if (!is.null(func)) {
            vals[[key]] <- func(var_tcltk, key)
        }
    }
    return(vals)
}

var_tcltk_get_message_variable <- function(var_tcltk, key) {
    return(var_tcltk[["message"]][[key]])
}

var_tcltk_set_message <- function(var_tcltk, key, mes) {
    tclvalue(var_tcltk[["message"]][[key]]) <- mes
}
