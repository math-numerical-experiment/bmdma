##' @description
##' ## Quantal linear
##' bmdma_model\[\["quantallinear"\]\] is the definition of quantal linear model:
##' \deqn{p(d) = x_{2}+\left(1-x_{2}\right)\,\left(1-e^ {-x_{1}\,d}\right).}
##' The parameters \eqn{(x_1, x_2)} are searched in
##' \eqn{[0 + \epsilon, \infty) \times [0, 1 - \epsilon]} by default.
##' @name bmdma_model
##' @rdname bmdma_model
NULL

bmdma_model[["quantallinear"]] <- list(
    ## g + (1 - g) * (1 - exp(-b * dose))
    ## x = (b, g)
    name = "quantal linear",
    number_of_parameters = 2,
    func = function(x, d) {
        return(x[2] + (1 - x[2]) * (1 - exp(-x[1] * d)))
    },
    grad = function(x, d) {
        v1 <- exp(-x[1] * d)
        return(c(d * (1 - x[2]) * v1, v1))
    },
    bmd = function(x, r) {
        if (x[1] == 0) {
            return(NA)
        }
        return(-log(1 - r) / x[1])
    },
    default_restriction = list(
        lower = c(0, 0),
        upper = c(Inf, 1),
        endpoint = list(
            lower = c(FALSE, TRUE),
            upper = c(TRUE, FALSE)
        )
    ),
    init = function(df, restriction) {
        ratio <- df[["response"]] / df[["N"]]
        ratio <- ratio[order(ratio)]
        dose <- df[["dose"]]
        dose <- dose[order(dose)]
        if (length(ratio) < 2) {
            return(c(-log(1 - ratio) / max(dose, 0.0001), 0.0001))
        }
        x2 <- max(ratio[1], 0.01)
        ratio_75 <- ratio[1] * 0.25 + ratio[length(ratio)] * 0.75
        v <- ratio - ratio_75
        i <- which(v > 0)[1]
        p <- v[i - 1] / (v[i - 1] - v[i])
        ratio_75 <- ratio[i - 1] * (1 - p) + ratio[i] * p
        dose_75 <- dose[i - 1] * (1 - p) + dose[i] * p
        x1 <- -(log((ratio_75 - 1) / (x2 - 1))) / dose_75
        prm <- c(x1, x2)
        if (prm[1] < 0.0) {
            prm[1] <- 0.0001
        }
        return(prm)
    }
)
