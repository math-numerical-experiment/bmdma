##' @import tcltk
##' @import yaml
##' @import stringr
##' @include utils.R
##' @include translator.R
##' @include model.R
##' @include gui_var.R
##' @include gui_func.R
NULL

##' Global variables for GUI
##' @noRd
gui_env <- new.env()

gui_env_initialize <- function(lang) {
    win <- tktoplevel()
    tkwm.withdraw(win)
    gui_env[["window"]] <- win
    gui_env[["translator"]] <- bmdma_translator_initialize(lang)
    gui_env[["contents"]] <- NULL
    gui_env[["data_analysis"]] <- NULL
    gui_env[["result"]] <- NULL
    gui_env[["var"]] <- gui_var_initialize(gui_env[["translator"]])
    gui_env[["tempfile"]] <- gui_prepare_temporary_files()
    gui_env[["reference_value_text"]] <- .list_reference_value_translation(gui_env[["translator"]])
}

gui_env_get_tcltk_message_variable <- function(key) {
    return(var_tcltk_get_message_variable(gui_env[["var"]][["tcltk"]], key))
}

gui_env_tcltk_message_set <- function(message_key, message = NULL) {
    if (is.null(message)) {
        message <- gui_env[["translator"]]$t(message_key)
    }
    var_tcltk_set_message(gui_env[["var"]][["tcltk"]], message_key, message)
}

gui_env_tcltk_message_reset <- function(message_key) {
    var_tcltk_set_message(gui_env[["var"]][["tcltk"]], message_key, "")
}

gui_env_get_tcltk_variable <- function(key) {
    return(gui_env[["var"]][["tcltk"]][[key]])
}

gui_env_read_data_file <- function() {
    data_analysis <- NULL
    path_data_analysis <- gui_env[["var"]][["value"]][["data_file"]]
    if (file.exists(path_data_analysis)) {
        gui_env_value_update("data_column")
        data_analysis <- bmdma_read_data_file(path_data_analysis)
        cols_default <- c("dose", "N", "response")
        cols_read <- gui_env[["var"]][["value"]][["data_column"]]
        for (col in cols_default) {
            if (!(col %in% cols_read)) {
                cols_read[col] <- col
            }
        }
        cols_data <- colnames(data_analysis)
        ind_exist <- names(cols_read) %in% cols_data
        if (any(!ind_exist)) {
            cols_rest <- cols_data[!(cols_data %in% names(cols_read))]
            i <- 1
            for (col in names(cols_read)) {
                if (!(col %in% cols_data)) {
                    if (i > length(cols_data)) {
                        break
                    }
                    cols_read[[cols_rest[[i]]]] <- cols_read[[col]]
                    cols_read[[col]] <- NULL
                    i <- i + 1
                }
            }
            if (i > 1) {
                gui_env_value_set(list("data_column" = cols_read))
            }
        }
        data_analysis <- bmdma_data_test(data_analysis, column = cols_read)
        if (is.null(data_analysis)) {
            gui_env_tcltk_message_set("main.label.invalid_data_file")
        } else {
            gui_env_tcltk_message_reset("main.label.invalid_data_file")
        }
    } else if (str_length(path_data_analysis) == 0) {
        gui_env_tcltk_message_set("main.label.invalid_data_file", gui_env[["translator"]]$t("main.label.no_data_file"))
    } else {
        gui_env_tcltk_message_set("main.label.invalid_data_file", gui_env[["translator"]]$t("main.label.nonexistent_data_file"))
    }
    gui_env[["data_analysis"]] <- data_analysis
    gui_env[["result"]] <- NULL
}

gui_env_value_set <- function(vals) {
    gui_env[["var"]] <- gui_var_set(gui_env[["var"]], vals)
    if (!is.null(vals[["data_file"]])) {
        gui_env_read_data_file()
    }
}

gui_env_value_update <- function(keys = NULL) {
    gui_env[["var"]] <- gui_var_update_from_tcltk(gui_env[["var"]], keys)
    sampling <- gui_env[["var"]][["value"]][["sampling"]]
    if (bmdma_config[["bootstrap"]][["sampling"]] != sampling) {
        bmdma_configure("set", c("bootstrap", "sampling"), sampling)
    }
}

gui_env_set <- function(key, val) {
    gui_env[[key]] <- val
}

gui_env_load_settings <- function(settings) {
    if (is.character(settings)) {
        if (file.exists(settings)) {
            settings <- yaml.load_file(settings)
        } else {
            settings <- NULL
        }
    }
    if (!is.null(settings)) {
        gui_env_value_set(settings)
    }
}

gui_env_save_settings <- function(path) {
    if (is.character(path) && str_length(path) > 0) {
        gui_env_value_update()
        write(as.yaml(gui_env[["var"]][["value"]]), path)
    }
}

gui_env_get_bmdma_ul_args <- function() {
    data_analysis <- gui_env[["data_analysis"]]
    if (is.null(data_analysis)) {
        return(NULL)
    }
    gui_env_value_update()
    args <- gui_var_bmdma_ul_args(gui_env[["var"]])
    args[["data_analysis"]] <- data_analysis
    return(args)
}

gui_env_update_contents <- function(tab_names = NULL) {
    data_analysis <- gui_env[["data_analysis"]]
    df_result <- gui_env[["result"]]
    gui_env[["contents"]] <- gui_contents_update(gui_env[["contents"]], data_analysis, df_result)
}
